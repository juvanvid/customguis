from PyQt4 import QtGui
from PyQt4.QtCore import *
from taurus.qt import QtCore
from PyTango import DbDatum

# Ram table view model
class RamTableViewModel(QAbstractTableModel):
    def __init__(self, screen, ramNumber, datain, headerdata, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.screen = screen
        self.arraydata = datain
        self.headerdata = headerdata
        self.ramNumber = ramNumber

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.arraydata) > 0:
            return len(self.arraydata[0])
        else:
            return 3

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:
            return QVariant()
        return QVariant(self.arraydata[index.row()][index.column()])

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if role == QtCore.Qt.EditRole:
            self.arraydata[index.row()][index.column()] = value.toPyObject()
        return False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])
        return QVariant()

    def flags(self, index):
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def addItem(self, item):
        self.beginInsertRows(QModelIndex(), len(self.arraydata), len(self.arraydata))
        self.arraydata.append(item)
        self.endInsertRows()

    def removeItem(self, itemNumber):
        if itemNumber.row() < 0:
            return

        self.beginRemoveRows(QModelIndex(), itemNumber.row(), itemNumber.row())
        self.arraydata.pop((itemNumber.row()))
        self.endRemoveRows()

    def clear(self):
        reset()

    # Create a table model from raw data
    @classmethod
    def createRamTableViewModel(ctl, screen, ramNumber, data):
        header = ["Event code", "Trigger pulse generators", "Device on output"]
        ramTableViewModel = RamTableViewModel(screen, ramNumber, data, header)
        return ramTableViewModel



# Business logic for RAM content.
class RamContentUpdater:

    # Load the GUI RAM content from the Tango database.
    @classmethod
    def loadDataFromDatabase(cls, screen, ramNumber, device):
        dataTable = getDataTableFromRamNumber(ramNumber, screen)

        # Load ramRecords property for given device.
        propertyName = "ramRecords"+str(ramNumber)
        properties = device.get_property(propertyName)
        ramRecords = properties[propertyName]

        # Create array of strings from property object.
        tableData = [record.split(' ') for record in ramRecords]

        # Load the records from the database.
        dataTable.setModel(RamTableViewModel.createRamTableViewModel(screen, ramNumber, tableData))

        print 'Ram content from property '+propertyName+' loaded successfully.'

    # Save the GUI RAM content to Tango database.
    @classmethod
    def saveRamContentToDatabase(cls, screen, ramNumber, device):
        dataTable = getDataTableFromRamNumber(ramNumber, screen).model().arraydata

        # Delete ramRecords property for given RAM.
        propertyName = "ramRecords"+str(ramNumber)
        device.delete_property(propertyName)

        property = DbDatum(propertyName)
        for ramRecord in dataTable:
            property.value_string.append(str(ramRecord[0]+' '+str(ramRecord[1])+' '+str(ramRecord[2])))

        device.put_property(property)

        print 'Ram content saved successfully to property '+propertyName+'.'

    # Load the GUI RAM content from file.
    @classmethod
    def loadRamContentFromFile(cls, screen, ramNumber):
        filename = QtGui.QFileDialog.getOpenFileName(screen, "Load file", '', '*.csv')
        if filename.length() == 0:
            # The user cancelled the action. Abort.
            return

        # Read line by line, create a list of lists.
        inputData = [line.rstrip('\n').split(' ') for line in open(filename, 'r')]

        dataTable = getDataTableFromRamNumber(ramNumber, screen)

        # Load the records from the database.
        dataTable.setModel(RamTableViewModel.createRamTableViewModel(screen, ramNumber, inputData))

        print 'File '+filename+' loaded successfully.'

    # Save the GUI RAM content to file.
    @classmethod
    def saveRamContentToFile(cls, screen, ramNumber):
        dataTable = getDataTableFromRamNumber(ramNumber, screen).model().arraydata

        # Load ramRecords property for given device.
        filename = QtGui.QFileDialog.getSaveFileName(screen, "Save file", "ramRecords"+str(ramNumber)+".csv", "*.csv")
        if filename.length() == 0:
            # User clicked cancel. Abort.
            return

        outputFile = open(filename, 'w')
        for record in dataTable:
            outputFile.write(str(record[0]+' '+str(record[1])+' '+str(record[2]))+'\n')
        outputFile.close()

        print 'File written to '+filename+' successfully.'


# Return the current data table for the specified RAM.
def getDataTableFromRamNumber(ramNumber, screen):
    if ramNumber == 0:
        return screen.ui.tableExpertRam0
    elif ramNumber == 1:
        return screen.ui.tableExpertRam1
    else:
        # There must be an error. Interrupt.
        print "Invalid ram number: "+str(ramNumber)
        raise "Invalid ram number: "+str(ramNumber)

# When the user clicks the Apply button, the ram content in the GUI is sent to the device server.
def performRAMUpdateOnDevice(screen, tangoDevice, ramNumber):
    dataTable = getDataTableFromRamNumber(ramNumber, screen).model().arraydata

    # Write to the ram map.
    for ramRecord in dataTable:
        tangoDevice.WriteSequenceEvent(str(ramNumber)+' '+str(ramRecord[0])+' '+str(ramRecord[1])+' '+str(ramRecord[2]))

    print 'Update for Ram'+str(ramNumber)+' completed successfully.'

# Enable or disable RAM.
def enableRam(ramNumber, device):
    if ramNumber == 0:
        device.SetRamState("1 0")
        device.SetRamState("0 1")
    elif ramNumber == 1:
        device.SetRamState("0 0")
        device.SetRamState("1 1")
    else:
        # There must be an error. Interrupt.
        print "Invalid ram number: "+str(ramNumber)
        raise "Invalid ram number: "+str(ramNumber)