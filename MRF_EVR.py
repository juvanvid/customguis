import signal
import traceback
from PyQt4.QtGui import *
from RAM_Communicator import *
from PG_Communicator import *
import EVR_expert
import argparse
import sys
import taurus

# Exception handling
def errorHandler(type, value, traceback):
    sys.__excepthook__(type, value, traceback)
    QMessageBox.warning(None, "An Exception occured", "Check the console for more details.", QMessageBox.Ok)
sys.excepthook = errorHandler

# CTRL-C handler
def exit_handler(signal, frame):
    print '\nBye.'
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)

# Execute command on device with parameters
def execute_tango_command(device_name, command_name, attribute, stringArgument):
    if attribute is None:
        getattr(taurus.Device(device_name), command_name)()
    else:
        if stringArgument:
            argument = str(attribute)
        else:
            argument = long(attribute)

        getattr(taurus.Device(device_name), command_name)(argument)


# Converts specific states to ram status number
def read_ram_status(recycle, running, enable, single):
    statusNumber = 0
    if single:
        statusNumber += 1
    if recycle:
        statusNumber += 2
    if enable:
        statusNumber += 4
    if running:
        statusNumber += 16
    return statusNumber

# Bind UI to Tango model
def bind_ui(device_name):
    tangoDevice = taurus.Device(arguments.tangoDeviceName)

    ##### Top of the window #####
    # Basic device info.
    evrUi.ui.labelDeviceName.setText(device_name)
    evrUi.ui.labelDeviceState.setModel(device_name + "/State")
    evrUi.ui.labelDeviceStatus.setModel(device_name + "/Status")

    evrUi.ui.buttonDeviceOff.setModel(device_name)
    evrUi.ui.buttonDeviceOff.setCommand("Off")

    evrUi.ui.buttonDeviceOn.setModel(device_name)
    evrUi.ui.buttonDeviceOn.setCommand("On")

    evrUi.ui.buttonDeviceReset.setModel(device_name)
    evrUi.ui.buttonDeviceReset.setCommand("Reset")

    evrUi.ui.buttonDeviceInit.setModel(device_name)
    evrUi.ui.buttonDeviceInit.setCommand("Init")

    # Violations.
    evrUi.ui.labelDeviceViolations.setModel(device_name + "/Violation")
    QtCore.QObject.connect(evrUi.ui.buttonClearViolations, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "ClearViolation", None, False))


    ##### Operator #####

    evrUi.ui.labelOperatorRam0Status.setModel(device_name + "/PulseMapState0")
    evrUi.ui.labelOperatorRam1Status.setModel(device_name + "/PulseMapState1")

    ##### Pulse Generators tab #####

    # Load and populate the pulse generator properties from the database.
    PulseGeneratorUpdater.loadDataFromDatabase(evrUi, tangoDevice)

    evrUi.ui.buttonGeneratorApply.clicked.connect(lambda: performPGUpdateOnDevice(evrUi, tangoDevice))
    evrUi.ui.buttonGeneratorReload.clicked.connect(lambda: PulseGeneratorUpdater.loadDataFromDatabase(evrUi, tangoDevice))
    evrUi.ui.buttonGeneratorSaveToDatabase.clicked.connect(lambda: PulseGeneratorUpdater.saveDataToDatabase(evrUi, tangoDevice))
    evrUi.ui.buttonGeneratorLoadFromFile.clicked.connect(lambda: PulseGeneratorUpdater.loadDataFromFile(evrUi))
    evrUi.ui.buttonGeneratorSaveToFile.clicked.connect(lambda: PulseGeneratorUpdater.saveDataToFile(evrUi))

    ##### Expert RAM tabs #####

    # Ram0.
    evrUi.ui.labelExpertRam0Status.setModel(device_name + "/PulseMapState0")

    evrUi.ui.buttonExpertRam0AddRow.clicked.connect(lambda: evrUi.ui.tableExpertRam0.model().addItem(["Event code", "Pulse generator", "Device name"]))
    evrUi.ui.buttonExpertRam0Apply.clicked.connect(lambda: performRAMUpdateOnDevice(evrUi, tangoDevice, 0))
    evrUi.ui.buttonExpertRam0Reload.clicked.connect(lambda: RamContentUpdater.loadDataFromDatabase(evrUi, 0, tangoDevice))
    evrUi.ui.buttonExpertRam0SaveToDatabase.clicked.connect(lambda: RamContentUpdater.saveRamContentToDatabase(evrUi, 0, tangoDevice))
    evrUi.ui.buttonExpertRam0Enable.clicked.connect(lambda: enableRam(0, tangoDevice))
    evrUi.ui.buttonExpertRam0Delete.clicked.connect(lambda: evrUi.ui.tableExpertRam0.model().removeItem(evrUi.ui.tableExpertRam0.currentIndex()))
    evrUi.ui.buttonExpertRam0LoadFromFile.clicked.connect(lambda: RamContentUpdater.loadRamContentFromFile(evrUi, 0))
    evrUi.ui.buttonExpertRam0SaveToFile.clicked.connect(lambda: RamContentUpdater.saveRamContentToFile(evrUi, 0))

    # Load and populate the ram content from the database.
    RamContentUpdater.loadDataFromDatabase(evrUi, 0, tangoDevice)

    # Fix the table header sizes.
    evrUi.ui.tableExpertRam0.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
    evrUi.ui.tableExpertRam0.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)

    # Ram1.
    evrUi.ui.labelExpertRam1Status.setModel(device_name + "/PulseMapState1")

    evrUi.ui.buttonExpertRam1AddRow.clicked.connect(lambda: evrUi.ui.tableExpertRam1.model().addItem(["Event code", "Pulse generator", "Device name"]))
    evrUi.ui.buttonExpertRam1Apply.clicked.connect(lambda: performRAMUpdateOnDevice(evrUi, tangoDevice, 1))
    evrUi.ui.buttonExpertRam1Reload.clicked.connect(lambda: RamContentUpdater.loadDataFromDatabase(evrUi, 1, tangoDevice))
    evrUi.ui.buttonExpertRam1SaveToDatabase.clicked.connect(lambda: RamContentUpdater.saveRamContentToDatabase(evrUi, 1, tangoDevice))
    evrUi.ui.buttonExpertRam1Enable.clicked.connect(lambda: enableRam(1, tangoDevice))
    evrUi.ui.buttonExpertRam1Delete.clicked.connect(lambda: evrUi.ui.tableExpertRam1.model().removeItem(evrUi.ui.tableExpertRam1.currentIndex()))
    evrUi.ui.buttonExpertRam0LoadFromFile.clicked.connect(lambda: RamContentUpdater.loadRamContentFromFile(evrUi, 1))
    evrUi.ui.buttonExpertRam0SaveToFile.clicked.connect(lambda: RamContentUpdater.saveRamContentToFile(evrUi, 1))

    # Load and populate the ram content from the database.
    RamContentUpdater.loadDataFromDatabase(evrUi, 1, tangoDevice)

    # Fix the table header sizes.
    evrUi.ui.tableExpertRam1.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
    evrUi.ui.tableExpertRam1.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)

    evrUi.ui.checkBoxAutoApplyPulseGenerators.setVisible(False)

class EVRUiWrapper(QMainWindow):

    def __init__(self, app):
        super(EVR_expert.QtGui.QMainWindow, self).__init__()

        self.app = app
        self.Form = EVR_expert.QtGui.QWidget()
        self.ui = EVR_expert.Ui_Form()

        try:
            # Setup UI and connect signals.
            self.ui.setupUi(self.Form)

        except Exception as e:
            QMessageBox.critical(self, 'Error while loading the GUI', 'Failed to initialize the GUI, please make sure the Tango device is running and are configured properly.\n\n' + str(traceback.format_exception(*sys.exc_info())))
            exit(1)

if __name__ == "__main__":

    # Read the tango device name passed as argument.
    parser = argparse.ArgumentParser()
    parser.add_argument("tangoDeviceName", help="Insert the fully qualified Tango device name", type=str)
    arguments = parser.parse_args()

    app = EVR_expert.QtGui.QApplication(sys.argv)
    evrUi = EVRUiWrapper(app)

    # Bind UI with Tango models
    bind_ui(arguments.tangoDeviceName)

    evrUi.Form.show()
    sys.exit(evrUi.app.exec_())