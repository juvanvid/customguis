from PyQt4 import QtCore
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QMessageBox, QHeaderView, QMainWindow
from RAM_Communicator import RamContentUpdater, performRAMUpdateOnDevice
import EVG_expert
import argparse
import sys
import signal
import PyTango
import taurus
import traceback

# Exception handling
def errorHandler(type, value, traceback):
    sys.__excepthook__(type, value, traceback)
    QMessageBox.warning(None, "An Exception occurred", "Check the console for more details.", QMessageBox.Ok)
sys.excepthook = errorHandler

# CTRL-C handler
def exit_handler(signal, frame):
    print '\nBye.'
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)

# Execute command on device with parameters
def execute_tango_command(device_name, command_name, attribute, stringArgument):
    if attribute is None:
        getattr(taurus.Device(device_name), command_name)()
    else:
        if stringArgument:
            argument = str(attribute)
        else:
            argument = long(attribute)

        getattr(taurus.Device(device_name), command_name)(argument)


# Converts specific states to ram status number
def convertRamStatusIntoLong(ramNumber, recycle, running, enable, single):
    statusNumber = 0
    if single:
        statusNumber += 1
    if recycle:
        statusNumber += 2
    if enable:
        statusNumber += 4
    if running:
        statusNumber += 16
    if ramNumber:
        statusNumber += 8
    return statusNumber

# Convert ram status into checkbox statuses on UI
def ram_status_changed(src, evt_type, attr_val):
    if not (isinstance(src, taurus.core.tango.tangoattribute.TangoAttribute) and evt_type == PyTango.EventType.CHANGE_EVENT):
        return
    ramNumber = int(attr_val.name[-1:])
    ramValue = attr_val.value
    evgUi.signalUpdateRamStatus.emit(ramNumber, ramValue)


# Updates the sequencer state for each ram.
def updateSequencerStateOnDevice(ramNumber, device_name):
    recycle = getattr(evgUi.ui, 'checkBoxExpertStatusRecycle'+str(ramNumber)).checkState()
    running = getattr(evgUi.ui, 'checkBoxExpertStatusRunning'+str(ramNumber)).checkState()
    enable = getattr(evgUi.ui, 'checkBoxExpertStatusEnable'+str(ramNumber)).checkState()
    single = getattr(evgUi.ui, 'checkBoxExpertStatusSingle'+str(ramNumber)).checkState()
    useMxc = getattr(evgUi.ui, 'checkBoxExpertOnMxc'+str(ramNumber)).checkState()

    result = str(convertRamStatusIntoLong(ramNumber, recycle, running, enable, single))
    if useMxc:
        result += " 0"

    deviceServer = taurus.Device(device_name)
    deviceServer.SetSequencerState(result)

# Bind the UI elements from the form.
def bind_ui(device_name):
    tangoDevice = taurus.Device(arguments.tangoDeviceName)

    ##### Top of the window #####
    # Basic device info.
    evgUi.ui.labelDeviceName.setText(device_name)
    evgUi.ui.labelDeviceState.setModel(device_name + "/State")
    evgUi.ui.labelDeviceStatus.setModel(device_name + "/Status")

    evgUi.ui.buttonDeviceOff.setModel(device_name)
    evgUi.ui.buttonDeviceOff.setCommand("Off")

    evgUi.ui.buttonDeviceOn.setModel(device_name)
    evgUi.ui.buttonDeviceOn.setCommand("On")

    evgUi.ui.buttonDeviceReset.setModel(device_name)
    evgUi.ui.buttonDeviceReset.setCommand("Reset")

    evgUi.ui.buttonDeviceInit.setModel(device_name)
    evgUi.ui.buttonDeviceInit.setCommand("Init")

    # Violations.
    evgUi.ui.labelDeviceViolations.setModel(device_name + "/Violation")
    QtCore.QObject.connect(evgUi.ui.buttonClearViolations, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "ClearViolation", None, False))

    # Send software event.
    QtCore.QObject.connect(evgUi.ui.buttonSWTriggerSend, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "SendSoftwareEvent", evgUi.ui.editSWTriggerCode.displayText(), False))

    #MXC0
    QtCore.QObject.connect(evgUi.ui.buttonMxcSet, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "SetMxcCounter", "0 "+str(evgUi.ui.editMxcPrescalar.displayText())+" "
                                                                                       +str(str(evgUi.ui.editMxcEventCode.displayText()))+" "
                                                                                       +str(int(evgUi.ui.checkBoxMxcEnable.isChecked())), True))

    ##### Operator #####

    evgUi.ui.labelOperatorRam0Status.setModel(device_name + "/StateSequence0")
    evgUi.ui.labelOperatorRam1Status.setModel(device_name + "/StateSequence1")

    ##### Expert RAM tabs #####

    # Ram0.
    evgUi.ui.labelExpertRam0Status.setModel(device_name + "/StateSequence0")

    QtCore.QObject.connect(evgUi.ui.buttonRam0ChangeStatus, QtCore.SIGNAL("clicked()"),
                           lambda: updateSequencerStateOnDevice(0, device_name))
    QtCore.QObject.connect(evgUi.ui.buttonExpertRam0Trigger, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "TriggerSequence", 0, False))
    QtCore.QObject.connect(evgUi.ui.buttonExpertRam0ResetAndDisable, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "ResetAndDisableSequencer", 0, False))

    evgUi.ui.buttonExpertRam0AddRow.clicked.connect(lambda: evgUi.ui.tableExpertRam0.model().addItem(["Address", "Timestamp", "Event code"]))
    evgUi.ui.buttonExpertRam0Apply.clicked.connect(lambda: performRAMUpdateOnDevice(evgUi, tangoDevice, 0))
    evgUi.ui.buttonExpertRam0Reload.clicked.connect(lambda: RamContentUpdater.loadDataFromDatabase(evgUi, 0, tangoDevice))
    evgUi.ui.buttonExpertRam0SaveToDatabase.clicked.connect(lambda: RamContentUpdater.saveDataToDatabase(evgUi, 0, tangoDevice))
    evgUi.ui.buttonExpertRam0Delete.clicked.connect(lambda: evgUi.ui.tableExpertRam0.model().removeItem(evgUi.ui.tableExpertRam0.currentIndex()))
    evgUi.ui.buttonExpertRam0LoadFromFile.clicked.connect(lambda: RamContentUpdater.loadDataFromFile(evgUi, 0))
    evgUi.ui.buttonExpertRam0SaveToFile.clicked.connect(lambda: RamContentUpdater.saveDataToFile(evgUi, 0))

    # Load and populate the ram content from the database.
    RamContentUpdater.loadDataFromDatabase(evgUi, 0, tangoDevice)

    # Fix the table header sizes.
    evgUi.ui.tableExpertRam0.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
    evgUi.ui.tableExpertRam0.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)

    # Ram1.
    evgUi.ui.labelExpertRam1Status.setModel(device_name + "/StateSequence1")

    QtCore.QObject.connect(evgUi.ui.buttonRam1ChangeStatus, QtCore.SIGNAL("clicked()"),
                           lambda: updateSequencerStateOnDevice(1, device_name))
    QtCore.QObject.connect(evgUi.ui.buttonExpertRam1Trigger, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "TriggerSequence", 1, False))
    QtCore.QObject.connect(evgUi.ui.buttonExpertRam1ResetAndDisable, QtCore.SIGNAL("clicked()"),
                           lambda: execute_tango_command(device_name, "ResetAndDisableSequencer", 1, False))

    evgUi.ui.buttonExpertRam1AddRow.clicked.connect(lambda: evgUi.ui.tableExpertRam1.model().addItem(["Address", "Timestamp", "Event code"]))
    evgUi.ui.buttonExpertRam1Apply.clicked.connect(lambda: performRAMUpdateOnDevice(evgUi, tangoDevice, 1))
    evgUi.ui.buttonExpertRam1Reload.clicked.connect(lambda: RamContentUpdater.loadDataFromDatabase(evgUi, 1, tangoDevice))
    evgUi.ui.buttonExpertRam1SaveToDatabase.clicked.connect(lambda: RamContentUpdater.saveDataToDatabase(evgUi, 1,tangoDevice))
    evgUi.ui.buttonExpertRam1Delete.clicked.connect(lambda: evgUi.ui.tableExpertRam1.model().removeItem(evgUi.ui.tableExpertRam1.currentIndex()))
    evgUi.ui.buttonExpertRam1LoadFromFile.clicked.connect(lambda: RamContentUpdater.loadDataFromFile(evgUi, 1))
    evgUi.ui.buttonExpertRam1SaveToFile.clicked.connect(lambda: RamContentUpdater.saveDataToFile(evgUi, 1))

    # Load and populate the ram content from the database.
    RamContentUpdater.loadDataFromDatabase(evgUi, 1, tangoDevice)

    # Fix the table header sizes.
    evgUi.ui.tableExpertRam1.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
    evgUi.ui.tableExpertRam1.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)

    # Connect RAM status changes with the UI checkbox changes
    taurus.Attribute(device_name + "/StateSequence0").addListener(ram_status_changed)
    taurus.Attribute(device_name + "/StateSequence1").addListener(ram_status_changed)



class EVGUiWrapper(QMainWindow):
    signalUpdateRamStatus = pyqtSignal(int, int, name="updateRamStatus")

    def __init__(self, app):
        super(EVG_expert.QtGui.QMainWindow, self).__init__()

        self.app = app
        self.Form = EVG_expert.QtGui.QWidget()
        self.ui = EVG_expert.Ui_Form()

        try:
            # Setup UI and connect signals.
            self.ui.setupUi(self.Form)
            self.signalUpdateRamStatus.connect(self.handleRamStatusUpdate)

        except Exception as e:
            QMessageBox.critical(self, 'Error while loading the GUI', 'Failed to initialize the GUI, please make sure the Tango device is running and are configured properly.\n\n' + str(traceback.format_exception(*sys.exc_info())))
            exit(1)

    def handleRamStatusUpdate(self, ramNumber, ramStatus):
        # bits = [isEnabled, isSingle, isRunning, isRecycle, sequncerNumber]
        bits = "{0:05b}".format(ramStatus)[::-1]

        getattr(self.ui, "checkBoxExpertStatusSingle"+str(ramNumber)).setChecked(int(bits[0]))
        getattr(self.ui, "checkBoxOperatorStatusSingle"+str(ramNumber)).setChecked(int(bits[0]))

        getattr(self.ui, "checkBoxExpertStatusRecycle"+str(ramNumber)).setChecked(int(bits[1]))
        getattr(self.ui, "checkBoxOperatorStatusRecycle"+str(ramNumber)).setChecked(int(bits[1]))

        getattr(self.ui, "checkBoxExpertStatusEnable"+str(ramNumber)).setChecked(int(bits[2]))
        getattr(self.ui, "checkBoxOperatorStatusEnable"+str(ramNumber)).setChecked(int(bits[2]))

        getattr(self.ui, "checkBoxExpertStatusRunning"+str(ramNumber)).setChecked(int(bits[4]))
        getattr(self.ui, "checkBoxOperatorStatusRunning"+str(ramNumber)).setChecked(int(bits[4]))


if __name__ == "__main__":

    # Read the tango device name passed as argument.
    parser = argparse.ArgumentParser()
    parser.add_argument("tangoDeviceName", help="Insert the fully qualified Tango device name", type=str)
    arguments = parser.parse_args()

    app = EVG_expert.QtGui.QApplication(sys.argv)
    evgUi = EVGUiWrapper(app)

    # Bind UI with Tango models
    bind_ui(arguments.tangoDeviceName)

    evgUi.Form.show()
    sys.exit(evgUi.app.exec_())
