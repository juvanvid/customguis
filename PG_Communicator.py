from PyQt4 import QtGui
from PyTango import DbDatum

class PulseGeneratorViewModel(dict):
    def __str__(self):
        return str(self.generatorNumber)+' '\
            +str(int(self.invertedPolarity))+' '\
            +str(int(self.enableReset))+' '\
            +str(int(self.enableSet))+' '\
            +str(int(self.enableTrigger))+' '\
            +str(int(self.enable))+' '\
            +str(self.prescalar)+' '\
            +str(self.delay)+' '\
            +str(self.width)+' '\
            +str(self.outputChannel)+' '\
            +str(self.deviceName)

# Takes care of updating the modified pulse generators on the device server.
class PulseGeneratorUpdater:

    # Load the PG content from the Tango database.
    @classmethod
    def loadDataFromDatabase(cls, screen, device):
        propertyName = "pulseGeneratorRecords"
        properties = device.get_property(propertyName)
        dataTable = [property.split(' ') for property in properties[propertyName]]

        resetGuiOfPulseGenerators(screen)
        setGuiOfPulseGenerators(dataTable, screen)

        print 'Pulse generators from property '+propertyName+' loaded successfully.'

    # Save the PG content to Tango database.
    @classmethod
    def saveDataToDatabase(ctl, screen, device):
        dataTable = getDataTableForPulseGenerators(screen)

        # Delete pulseGeneraotr property PG.
        propertyName = "pulseGeneratorRecords"
        device.delete_property(propertyName)

        property = DbDatum(propertyName)
        # Update all the modified pulse generators.
        for pulseGenerator in dataTable:
            property.value_string.append(str(pulseGenerator))

        device.put_property(property)

        print 'Pulse generators saved successfully to property '+propertyName+'.'

    # Load the Pulse generators content from file.
    @classmethod
    def loadDataFromFile(cls, screen):
        filename = QtGui.QFileDialog.getOpenFileName(screen, "Load file", '', '*.csv')
        if filename.length() == 0:
            # The user cancelled the action. Abort.
            return

        # Read line by line, create a list of lists.
        inputData = [line.rstrip('\n').split(' ') for line in open(filename, 'r')]

        resetGuiOfPulseGenerators(screen)
        setGuiOfPulseGenerators(inputData, screen)

        print 'File '+filename+' loaded successfully.'

    # Save the Pulse generators content to file.
    @classmethod
    def saveDataToFile(cls, screen):
        dataTable = getDataTableForPulseGenerators(screen)

        # Load ramRecords property for given device.
        filename = QtGui.QFileDialog.getSaveFileName(screen, "Save file", "pulseGenerators.csv", "*.csv")
        if filename.length() == 0:
            # User clicked cancel. Abort.
            return

        outputFile = open(filename, 'w')
        for record in dataTable:
            outputFile.write(str(record)+'\n')
        outputFile.close()

        print 'File written to '+filename+' successfully.'


# Return the current data table for the pulse generators.
def getDataTableForPulseGenerators(screen):
    dataTable = []
    for i in range(0, 12):
        pulseGenerator = PulseGeneratorViewModel()

        # Generator number.
        pulseGenerator.generatorNumber = i

        # Properties.
        pulseGenerator.invertedPolarity = getattr(screen.ui, 'checkBoxGeneratorInvertedPolarity'+str(i)).isChecked()
        pulseGenerator.enableReset = getattr(screen.ui, 'checkBoxGeneratorReset'+str(i)).isChecked()
        pulseGenerator.enableSet = getattr(screen.ui, 'checkBoxGeneratorSet'+str(i)).isChecked()
        pulseGenerator.enableTrigger = getattr(screen.ui, 'checkBoxGeneratorTrigger'+str(i)).isChecked()
        pulseGenerator.enable = getattr(screen.ui, 'checkBoxGeneratorEnable'+str(i)).isChecked()

        # Parameters.
        pulseGenerator.prescalar = getattr(screen.ui, 'editGeneratorPrescalar'+str(i)).text()
        pulseGenerator.delay = str(getattr(screen.ui, 'editGeneratorDelay'+str(i)).value())
        pulseGenerator.width = getattr(screen.ui, 'editGeneratorWidth'+str(i)).text()

        # Output mapping.
        pulseGenerator.outputChannel = getattr(screen.ui, 'editGeneratorOutput'+str(i)).text()
        pulseGenerator.deviceName = getattr(screen.ui, 'editGeneratorOutputDeviceName'+str(i)).text()

        # Append only the pulse generators that have the correct settings.
        if pulseGenerator.prescalar and pulseGenerator.delay and pulseGenerator.width and pulseGenerator.outputChannel and pulseGenerator.deviceName:
            dataTable.append(pulseGenerator)

    return dataTable


 # When the user clicks the Apply button, the changes made in the client are sent to the device server.
def performPGUpdateOnDevice(screen, tangoDevice):
    dataTable = getDataTableForPulseGenerators(screen)

    # Update all the modified pulse generators.
    for pulseGenerator in dataTable:

        # Save pulse generator properties to device.
        tangoDevice.SetPulseGeneratorProperties(str(pulseGenerator.generatorNumber)+' '+str(int(pulseGenerator.invertedPolarity))+' '+
                                               str(int(pulseGenerator.enableReset))+' '+str(int(pulseGenerator.enableSet))+' '+
                                               str(int(pulseGenerator.enableTrigger))+' '+str(int(pulseGenerator.enable)))

        # Save pulse generator parameters to device.
        tangoDevice.SetPulseGeneratorParameters(str(pulseGenerator.generatorNumber)+' '+str(pulseGenerator.prescalar)+' '+
                                               str(pulseGenerator.delay)+' '+str(pulseGenerator.width))

        # Save output mapping to device.
        tangoDevice.SetPulseGeneratorToOutputMap(str(pulseGenerator.generatorNumber)+' '+str(pulseGenerator.outputChannel))

    print 'Pulse generators update completed successfully.'


# Reset the current Pulse Generators GUI state.
def resetGuiOfPulseGenerators(screen):
    # Reset the current UI state
    for i in range(0, 12):
        getattr(screen.ui, 'checkBoxGeneratorInvertedPolarity'+str(i)).setChecked(False)
        getattr(screen.ui, 'checkBoxGeneratorReset'+str(i)).setChecked(False)
        getattr(screen.ui, 'checkBoxGeneratorSet'+str(i)).setChecked(False)
        getattr(screen.ui, 'checkBoxGeneratorTrigger'+str(i)).setChecked(False)
        getattr(screen.ui, 'checkBoxGeneratorEnable'+str(i)).setChecked(False)

        # Parameters.
        getattr(screen.ui, 'editGeneratorPrescalar'+str(i)).setText("")
        getattr(screen.ui, 'editGeneratorDelay'+str(i)).setValue(0)
        getattr(screen.ui, 'editGeneratorWidth'+str(i)).setText("")

        # Output mapping.
        getattr(screen.ui, 'editGeneratorOutput'+str(i)).setText("")
        getattr(screen.ui, 'editGeneratorOutputDeviceName'+str(i)).setText("")


# Set the current Pulse Generators GUI state
def setGuiOfPulseGenerators(dataTable, screen):
    # Populate the UI with the records from the database
    for pulseGenerator in dataTable:
        generatorNumber = pulseGenerator[0]

        # Properties.
        getattr(screen.ui, 'checkBoxGeneratorInvertedPolarity'+str(generatorNumber)).setChecked(int(pulseGenerator[1]))
        getattr(screen.ui, 'checkBoxGeneratorReset'+str(generatorNumber)).setChecked(int(pulseGenerator[2]))
        getattr(screen.ui, 'checkBoxGeneratorSet'+str(generatorNumber)).setChecked(int(pulseGenerator[3]))
        getattr(screen.ui, 'checkBoxGeneratorTrigger'+str(generatorNumber)).setChecked(int(pulseGenerator[4]))
        getattr(screen.ui, 'checkBoxGeneratorEnable'+str(generatorNumber)).setChecked(int(pulseGenerator[5]))

        # Parameters.
        getattr(screen.ui, 'editGeneratorPrescalar'+str(generatorNumber)).setText(pulseGenerator[6])
        getattr(screen.ui, 'editGeneratorDelay'+str(generatorNumber)).setValue(int(pulseGenerator[7]))
        getattr(screen.ui, 'editGeneratorWidth'+str(generatorNumber)).setText(pulseGenerator[8])

        # Output mapping.
        getattr(screen.ui, 'editGeneratorOutput'+str(generatorNumber)).setText(pulseGenerator[9])
        getattr(screen.ui, 'editGeneratorOutputDeviceName'+str(generatorNumber)).setText(pulseGenerator[10])