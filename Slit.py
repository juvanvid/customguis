import sys
import taurus
import re

from PyQt4 import QtGui

from taurus.qt.qtgui.extra_pool import PoolMotorTV
from taurus.qt.qtgui.panel import TaurusForm


SLIT = "SLT"
MIRROR = "MIR"
MONOCHR = "MON"


class InstrumentDevice:
    def __init__(self, type, namePattern):
        self.type = type
        self.namePattern = namePattern
        self.subElements = []


def setupSlit(slitName, slitConfig, outSubSlits):
    match = re.search(slitConfig.namePattern, slitName)
    baseName = match.group(1)
    enum = match.group(2)

    outSubSlits.append(baseName + "X" + enum)
    outSubSlits.append(baseName + "Y" + enum)


deviceName = str(sys.argv[1])
if deviceName.index("/") > 0:
    deviceName = deviceName.split("/")[-1]

app = QtGui.QApplication(sys.argv)
form = TaurusForm()
form.setWindowTitle(deviceName)


instrumentTypes = {
    SLIT: InstrumentDevice(SLIT, r"(^.+SLT)(\d+)"),
    MIRROR: InstrumentDevice(MIRROR, r"MON"),
    MONOCHR: InstrumentDevice(MONOCHR, r"MIR\d+")
}

controllers = []
composedInstruments = [instr for k, instr in instrumentTypes.iteritems() if re.search(instr.namePattern, deviceName)]
if composedInstruments:
    instrument = composedInstruments[0]

    if instrument.type == SLIT:
        setupSlit(deviceName, instrument, controllers)

    else:
        controllers = [deviceName]

else:
    controllers = [deviceName]


motors = []
#motors = ["BL-05ID-OPT-GAPX1", "BL-05ID-OPT-OFFX1", "BL-05ID-OPT-GAPY1", "BL-05ID-OPT-OFFY1"]

#ctrls = []
#ctrls = ["BL-05ID-OPT-SLTX1", "BL-05ID-OPT-SLTY1"]


for motor in motors:
    widget = PoolMotorTV(form.scrollArea)
    widget.setModel(motor)

for ctrl in controllers:
    ctlProxy = taurus.Device(ctrl)

    elements = ctlProxy.ElementList
    for el in elements:
        widget = PoolMotorTV(form.scrollArea)
        widget.setModel(el)

form.show()
sys.exit(app.exec_())