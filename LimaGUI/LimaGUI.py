
import sys
import MainWindow
from PyQt4 import QtGui
from taurus.qt.qtgui.application import TaurusApplication
import PyTango

__author__ = "Vid Juvan"
__email__ = "vid.juvan@cosylab.com"
__status__ = "Prototype"


if len(sys.argv) < 2:
    print "Usage: LimaGUI.py device_proxy"
    sys.exit(1)

proxs = ["","","","","","",""]

info = PyTango.DeviceProxy(sys.argv[1]).info()
database = PyTango.Database()
devices = database.get_device_class_list(info.server_id)
for i in range(1, len(devices)):
    if devices[i] == "LimaCCDs":
        proxs[0] = devices[i-1]
    elif devices[i] == "Roi2spectrumDeviceServer":
        proxs[1] = devices[i-1]
    elif devices[i] == "FlatfieldDeviceServer":
        proxs[2] = devices[i-1]
    elif devices[i] == "BackgroundSubstractionDeviceServer":
        proxs[3] = devices[i-1]
    elif devices[i] == "RoiCounterDeviceServer":
        proxs[4] = devices[i-1]
    elif devices[i] == "MaskDeviceServer":
        proxs[5] = devices[i-1]
    elif devices[i] == "LiveViewer":
        proxs[6] = devices[i-1]


app = TaurusApplication([])
app.setStyle(QtGui.QStyleFactory.create("Cleanlooks"))

mainWindow = QtGui.QWidget()
ui = MainWindow.Ui_Form()

ui.setupUi(mainWindow, proxs)

mainWindow.show()
ret = app.exec_()
sys.exit(ret)